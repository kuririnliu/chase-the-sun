package main

import (
	"fmt"
	"os"

	"github.com/kataras/iris/v12"
)

func main() {
	app := iris.New()
	// Load all templates from the "./views" folder
	// where extension is ".html" and parse them
	// using the standard `html/template` package.
	path, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(path)
	app.RegisterView(iris.HTML("../../../ui", ".html"))

	// Method:    GET
	// Resource:  http://localhost:8080
	app.Get("/", func(ctx iris.Context) {
		// Bind: {{.messge}} with "Hello world!"
		ctx.ViewData("message", path)
		// Render template file: ./views/hello.html
		ctx.View("hello.html")
	})

	// Method:    GET
	// Resource:  http://localhost:8080/user/42
	//
	// Need to use a custom regexp instead?
	// Easy;
	// Just mark the parameter's type to 'string'
	// which accepts anything and make use of
	// its `regexp` macro function, i.e:
	// app.Get("/user/{id:string regexp(^[0-9]+$)}")
	app.Get("/user/{id:uint64}", func(ctx iris.Context) {
		userID, _ := ctx.Params().GetUint64("id")
		ctx.Writef("User ID: %d", userID)
	})

	app.Post("/checkout", func(ctx iris.Context) {
		ctx.WriteString("post response")
		b, err := ctx.GetBody()

		if err != nil {
			ctx.StatusCode(iris.StatusBadRequest)
			ctx.WriteString(err.Error())
			return
		}
		c, err := ctx.
			ctx.Writef("Received: %s\n", string(b))
	})

	// Start the server using a network address.
	app.Run(iris.Addr(":8080"))
}
