module gitlab.com/kuririnliu/chase-the-sun/m

go 1.14

require (
	github.com/CloudyKit/fastprinter v0.0.0-20200109182630-33d98a066a53 // indirect
	github.com/kataras/iris/v12 v12.0.0
	github.com/klauspost/compress v1.12.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
)
